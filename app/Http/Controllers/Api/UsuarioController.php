<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use Symfony\Component\HttpFoundation\Request;

class UsuarioController extends Controller
{
    public function getUsuarios()
    {
        $usuarios = User::all();

        return $usuarios->toJson();
    }

    public function cadastrarUsuario(Request $request)
    {
    }
}
